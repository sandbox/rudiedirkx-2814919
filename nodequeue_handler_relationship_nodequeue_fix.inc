<?php

/**
 * Sub class for Nodequeue's relationship, to make it work with smartqueues.
 */

class nodequeue_handler_relationship_nodequeue_fix extends nodequeue_handler_relationship_nodequeue {

  /**
   * Override nodequeue_handler_relationship_nodequeue::query().
   */
  function query() {
    // Get all queues.
    $queues = nodequeue_load_queues(nodequeue_get_all_qids(0, 0, TRUE), TRUE);
    $map = nodequeue_get_qid_map();

    // Cycle through all selected queues (hopefully 1).
    $sqids = array();
    foreach (array_filter($this->options['names']) as $queue) {
      $queue = $queues[ $map[$queue] ];

      // Find relevant subqueues for this queue.
      $more_sqids = module_invoke($queue->owner, 'nodequeue_sqids', $queue);
      if (!$more_sqids) {
        // Or load all subqueues if owner doesn't know 'nodequeue_sqids'.
        $more_sqids = db_query('SELECT sqid FROM {nodequeue_subqueue} WHERE qid = ?', array($queue->qid))->fetchCol();
      }

      // Add to subqueue list.
      $sqids = array_merge($sqids, array_values($more_sqids));
    }

    // Let Nodequeue do its JOIN thing. It's almost perfect.
    parent::query();

    // Replace 'qid' condition with smart 'sqid'.
    $join = $this->query->table_queue[$this->alias]['join'];
    $join->extra[0] = array(
      'field' => 'sqid',
      'value' => $sqids,
    );
  }

}
