<?php

/**
 * Implements hook_nodequeue_sqids().
 *
 * @return array[int]
 */
function hook_nodequeue_sqids($queue) {
  global $user;

  if (!empty($user->uid)) {
    $reference = $user->name;
    $subqueues = nodequeue_load_subqueues_by_reference(array($queue->qid => array($reference)));
    $sqids = array_keys($subqueues);
    return $sqids;
  }

  // Anonymous must not see everything.
  return array(0);
}
